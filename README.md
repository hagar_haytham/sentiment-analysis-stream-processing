# Sentiment Analysis Stream Processing
Real Time NLP Preprocessing For Sentiment Analysis using Kafka Streams API.

## Build And Run
```sh
>> mvn clean package
>> mvn exec:java -Dexec.mainClass="com.barq.abs.kafka.streams.sentimentAnalysis.SocialMediaStreamManipulation"
```
