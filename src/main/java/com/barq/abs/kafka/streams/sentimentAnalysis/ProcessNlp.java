package com.barq.abs.kafka.streams.sentimentAnalysis;


import org.deeplearning4j.nn.modelimport.keras.preprocessing.text.KerasTokenizer;
import opennlp.tools.stemmer.PorterStemmer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class ProcessNlp {
    public static ArrayList<String> punctuations = new ArrayList<String>(Arrays.asList("@", "[", "]", "(", ")", ",", ".", "/", "_", "-", ":", "?"));
    public static ArrayList<String> digits = new ArrayList<String>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
    private static PorterStemmer stemmer = new PorterStemmer();

    public static ArrayList<Integer> padFeatures(ArrayList<Integer> sequence, int maxRowSize) {
        ArrayList<Integer> paddedSequence = new ArrayList<>(maxRowSize);
        int sequenceSize = sequence.size();

        if (sequenceSize <= maxRowSize) {
            int index = 0;
            for (int i = 0; i < maxRowSize; i++) {
                if (i < maxRowSize-sequenceSize)
                    paddedSequence.add(0);
                else
                    paddedSequence.add(sequence.get(index++));
            }
        }
        else if (sequenceSize > maxRowSize) {
            paddedSequence = (ArrayList<Integer>) sequence.subList(0, maxRowSize);
        }

        return paddedSequence;
    }

    public static ArrayList<Integer> tokensToSequence(ArrayList<String> tokens) {
        WordDictionary.contructDict();
        int maxRowSize = 80;

        ArrayList<Integer> sequence = new ArrayList<>();

        for (int i = 0; i < tokens.size(); i++) {
            Integer r = WordDictionary.wordDict.get(tokens.get(i));
            if (r != null)
                sequence.add(r);
        }

        return padFeatures(sequence, maxRowSize);
    }

    public static String removeSpecialCharacters(String rawText) {
        return rawText.replaceAll("[^\\p{ASCII}]", "");
    }

    public static ArrayList<String> removeStopWords(ArrayList<String> words) {
        if (!StopWords.isLower)
            StopWords.lower();

        ArrayList<String> cleanWords = new ArrayList<>();
        for(int i = 0; i < words.size(); i++) {
            if (!StopWords.stopWordsOfWordnet.contains(words.get(i))) {
                cleanWords.add(words.get(i));
            }
        }

        return cleanWords;
    }

    public static ArrayList<String> removePunctuation(ArrayList<String> words) {
        if (!StopWords.isLower)
            StopWords.lower();

        ArrayList<String> cleanWords = new ArrayList<>();
        for(int i = 0; i < words.size(); i++) {
            if (!punctuations.contains(words.get(i))) {
                cleanWords.add(words.get(i));
            }
        }

        return cleanWords;
    }

    public static ArrayList<String> removeDigit(ArrayList<String> words) {
        if (!StopWords.isLower)
            StopWords.lower();

        ArrayList<String> cleanWords = new ArrayList<>();
        for(int i = 0; i < words.size(); i++) {
            if (!digits.contains(words.get(i))) {
                cleanWords.add(words.get(i));
            }
        }

        return cleanWords;
    }

    public static ArrayList<String> stem(List<String> words) {
        ArrayList<String> stemmedWords = new ArrayList<String>();

        for (String s : words) {
            stemmedWords.add(stemmer.stem(s));
        }

        return stemmedWords;
    }

    public static Integer[] process(String rawText) {
        rawText = rawText.toLowerCase();
        String [] tokens = KerasTokenizer.textToWordSequence(rawText, "!\"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n", true, " ");

        ArrayList<String> tokensList = new ArrayList<>();
        for (int i = 0; i < tokens.length; i++) {
            tokensList.add(tokens[i]);
        }

        ArrayList<String> filteredTokens = removeDigit(removePunctuation(removeStopWords(tokensList)));
        ArrayList<Integer> processed = tokensToSequence(stem(filteredTokens));


        return processed.toArray(new Integer[0]);
    }


    public static Integer getSize(String message){
        String [] tokens = KerasTokenizer.textToWordSequence(message, "!\"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n", true, " ");
        return tokens.length;
    }
//    public static void main(String[] args) {
//        String testSample = "the left photo is the test i took before i get to know X1. and i took the right one a minute ago. when i said X1 ha\u2026 https://t.co/8ASDlM7fSE";
//        Integer[] result = process(testSample);
//
//        for (int i = 0; i < 100; i++)
//            System.out.println(result[i]);
//    }
}
