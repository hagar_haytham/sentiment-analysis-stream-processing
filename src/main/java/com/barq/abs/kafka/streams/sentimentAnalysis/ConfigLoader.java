package com.barq.abs.kafka.streams.sentimentAnalysis;

import org.nd4j.shade.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;


public class ConfigLoader {
    private static ConfigLoader instance = null;
    private static final String FILE_NAME = "config.json";

    private ConfigLoader() {}

    public static void loadConfig() {
        if (instance == null) {
            synchronized(ConfigLoader.class) {
                instance = new ConfigLoader();
                File dictFile = instance.getFileFromResources(FILE_NAME);
                instance.getData(dictFile);
            }
        }
    }

    // get file from classpath, resources folder
    private File getFileFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }

    private void getData(File file) {
        if (file == null)
            return;

        try {
            ObjectMapper mapper = new ObjectMapper();
            Config.setConfig(mapper.readValue(file, Config.class));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        ConfigLoader.loadConfig();
        Config.getInstance().print();
    }
}
