package com.barq.abs.kafka.streams.sentimentAnalysis;

public class Config {
    public String[] inputTopics;
    public String outputTopic;
    public String brokers;
    public String applicationId;
    public String clientId;
    public String sentimentAnalysisServiceUrl;

    private static Config instance = null;

    public static void setConfig(Config config) {
        if (instance  == null) {
            synchronized(ConfigLoader.class) {
                instance = config;
            }
        }
    }

    public static Config getInstance() {
        return instance;
    }

    public void print() {
        System.out.println(inputTopics);
        System.out.println(brokers);
        System.out.println(outputTopic);
        System.out.println(applicationId);
        System.out.println(clientId);
        System.out.println(sentimentAnalysisServiceUrl);
    }
}
